-- Challenge Bonus queries.
-- 1. (2.5 pts)
-- Retrieve all the number of backer_counts in descending order for each `cf_id` for the "live" campaigns. 
SELECT * FROM campaign

DROP TABLE live_backers

SELECT cf_id, backers_count
INTO live_backers
FROM campaign
WHERE outcome = 'live'
ORDER BY backers_count DESC;

SELECT * FROM LIVE_BACKERS



-- 2. (2.5 pts)
-- Using the "backers" table confirm the results in the first query.
DROP TABLE CONFIRMED_BACKER_COUNT

SELECT live_backers.cf_id, live_backers.backers_count, COUNT(backers.backer_id )
INTO confirmed_backer_count
FROM live_backers, backers
WHERE live_backers.cf_id = backers.cf_id
GROUP BY live_backers.cf_id, live_backers.backers_count

SELECT * FROM confirmed_backer_count
-- 3. (5 pts)
-- Create a table that has the first and last name, and email address of each contact.
-- and the amount left to reach the goal for all "live" projects in descending order. 
DROP TABLE contact_remaining

SELECT contacts.first_name, contacts.last_name, contacts.email, (campaign.goal-campaign.pledged) AS remaining
INTO contact_remaining
FROM campaign
JOIN contacts on contacts.contact_id = campaign.contact_id
WHERE campaign.outcome = 'live'
ORDER BY remaining DESC



-- Check the table
SELECT * FROM contact_remaining

-- 4. (5 pts)
-- Create a table, "email_backers_remaining_goal_amount" that contains the email address of each backer in descending order, 
-- and has the first and last name of each backer, the cf_id, company name, description, 
-- end date of the campaign, and the remaining amount of the campaign goal as "Left of Goal". 
DROP TABLE backers_remaining

SELECT backers.email, backers.first_name, backers.last_name, campaign.cf_id, campaign.company_name, campaign.description,  (campaign.goal-campaign.pledged) AS remaining
INTO backers_remaining
FROM campaign
JOIN backers on backers.cf_id = campaign.cf_id
WHERE campaign.outcome = 'live'
ORDER BY email ASC


-- Check the table
SELECT * FROM backers_remaining